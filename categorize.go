package main

import (
	"flag"
	"fmt"
	"math"
	"sort"

	"github.com/dustin/go-humanize"
)

type GoCat struct {
	logger *AppLogger
}

func (c *GoCat) Synopsis() string {
	return "Add todo task to list"
}

func (c *GoCat) Help() string {
	return "Usage: hoge fuag"
}

func (c *GoCat) Run(args []string) int {
	var flagLogButtom float64
	var flagMesh int
	var flagUnit string
	var flagOfs string
	flags := flag.NewFlagSet("goCat", flag.ContinueOnError)
	flags.IntVar(&flagMesh, "mesh", 10, "todo")
	flags.IntVar(&flagMesh, "m", 10, "todo")
	flags.Float64Var(&flagLogButtom, "bottom", 3.141, "todo")
	flags.Float64Var(&flagLogButtom, "b", 3.141, "todo")
	flags.StringVar(&flagUnit, "unit", "ms", "todo")
	flags.StringVar(&flagUnit, "u", "ms", "todo")
	flags.StringVar(&flagOfs, "ofs", "\t", "set output data format")
	flags.StringVar(&flagOfs, "o", "\t", "set output data format")
	if err := flags.Parse(args); err != nil {
		c.logger.FatalLog(err)
		return ExitCodeError
	}

	// Load Data fron os.Stdin
	xs, err := loadFloat64DataSeries()
	if err != nil {
		c.logger.FatalLog(err)
		return ExitCodeError
	}

	var i int
	grid := make([]float64, flagMesh)
	for i < flagMesh {
		grid[i] = math.Pow(flagLogButtom, float64(i+1))
		i++
	}
	// sort data series
	sort.Float64s(xs)
	result := CategoryMap{}
	var sum int
	var pos int
	for i, v := range grid {
		var count int
		index, _ := boundaryIdx(xs, pos, v)
		if index > 0 {
			count = index - pos
			pos = index
		} else {
			count = 0
		}
		d := &CategoryData{
			idx:   i,
			label: fmt.Sprintf("p < %s%s", humanize.Ftoa(v), flagUnit),
			count: count,
		}
		result = append(result, d)
		sum = sum + count
	}
	//
	var count int
	if sum < len(xs) {
		count = len(xs) - sum
	}
	d := &CategoryData{
		idx:   len(grid),
		label: fmt.Sprintf("p >= %s%s", humanize.Ftoa(grid[len(grid)-1]), flagUnit),
		count: count,
	}
	result = append(result, d)
	result.PrettyPrinted(c.logger, flagOfs)
	return 0
}

// CategoryData
type CategoryData struct {
	idx   int
	label string
	count int
}

func (c *CategoryData) PrettyPrinted(logger *AppLogger, ofs string) {
	var format string
	switch ofs {
	case "\t":
		format = "%s\t%d\n"
	case ",":
		format = "\"%s\",\"%d\"\n"
	default:
		format = "%s\t%d\n"
	}
	logger.Log(format, c.label, c.count)
}

// CategoryMap
type CategoryMap []*CategoryData

// Len
func (c CategoryMap) Len() int {
	return len(c)
}

// Swap
func (c CategoryMap) Swap(i, j int) {
	c[i], c[j] = c[j], c[i]
}

// Less
func (c CategoryMap) Less(i, j int) bool {
	return c[i].idx < c[j].idx
}

// PrettyPrinted
func (c CategoryMap) PrettyPrinted(logger *AppLogger, ofs string) {
	// sort data
	sort.Sort(c)
	for _, v := range c {
		v.PrettyPrinted(logger, ofs)
	}
}

func boundaryIdx(xs []float64, index int, boundary float64) (int, error) {
	var pos int
	for i := index; i < len(xs); i++ {
		if xs[i] > boundary {
			pos = i
			break
		}
	}
	return pos, nil
}
