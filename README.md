gosti
---

A CLI tool that receives a data string (floating point) passed from a pipe and calculates statistical values ​​(average value, median value, 90 th percentile value, 99 th percentile value) and writes the result to standard output

## Usage

~~~
$ grep "" data.txt | gosti -l "label"
~~~

or

~~~
$ gosti -l "label" < "data.txt"
~~~


### output

|No. | Type | Description|
|:------|:-------------|:------------------------|
|`count` | `int` | number of data series element |
|`average` | `float64` | average value of data series |
|`50%ile` | `float64` | the median value of data series |
|`90%ile` | `float64` | the 90%ile value of data series |
|`95%ile` | `float64` | the 95%ile value of data series |
|`99%ile` | `float64` | the 99%ile value of data series |
|`standard ` | `float64` | standard deviation of data series|

.