package main

import (
	"flag"
	"math"
	"sort"

	"github.com/dustin/go-humanize"
)

type Gosti struct {
	logger *AppLogger
}

// Synopsis satisfied michellh/chi
func (c *Gosti) Synopsis() string {
	return "Add todo task to list"
}

// Help satisfied micchell/chi
func (c *Gosti) Help() string {
	return "Usage: hoge fuag"
}

// Run satisfied micchell/chi
func (c *Gosti) Run(args []string) int {
	var flagLabel string
	var flagOfs string
	flags := flag.NewFlagSet("gosti", flag.ContinueOnError)
	flags.StringVar(&flagLabel, "label", "-", "add output data label")
	flags.StringVar(&flagLabel, "l", "-", "add output data label")
	flags.StringVar(&flagOfs, "ofs", "\"\t\"", "set output data format")
	flags.StringVar(&flagOfs, "o", "\"\t\"", "set output data format")
	if err := flags.Parse(args); err != nil {
		c.logger.FatalLog(err)
		return ExitCodeError
	}

	// Load Data fron os.Stdin
	xs, err := loadFloat64DataSeries()
	if err != nil {
		c.logger.FatalLog(err)
		return ExitCodeError
	}

	//
	stat := &Stat{
		label:             flagLabel,
		total:             len(xs),
		average:           average64(xs),
		percentil50:       percentile64(xs, 50.0),
		percentil90:       percentile64(xs, 90.0),
		percentil95:       percentile64(xs, 95.0),
		percentil99:       percentile64(xs, 99.0),
		standardDeviation: standardDeviation64(xs),
	}
	stat.PrettyPrinted(c.logger, flagOfs)
	return ExitCodeOK
}

type Stat struct {
	label             string
	total             int
	average           float64
	percentil50       float64
	percentil90       float64
	percentil95       float64
	percentil99       float64
	standardDeviation float64
}

func (s *Stat) PrettyPrinted(logger *AppLogger, ofs string) {
	var format string
	switch ofs {
	case "\t":
		format = "%s\t%d\t%s\t%s\t%s\t%s\t%s\t%s\n"
	case ",":
		format = "\"%s\",\"%d\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\"\n"
	default:
		format = "%s\t%d\t%s\t%s\t%s\t%s\t%s\t%s\n"
	}
	logger.Log(format,
		s.label,
		s.total,
		humanize.Ftoa(s.average),
		humanize.Ftoa(s.percentil50),
		humanize.Ftoa(s.percentil90),
		humanize.Ftoa(s.percentil95),
		humanize.Ftoa(s.percentil99),
		humanize.Ftoa(s.standardDeviation))
}

// average64
func average64(xs []float64) float64 {
	total := 0.0
	for _, v := range xs {
		total += v
	}
	return total / float64(len(xs))
}

// standardDeviation64
func standardDeviation64(xs []float64) float64 {
	avg := average64(xs)
	div := 0.0
	for _, v := range xs {
		div += (v - avg) * (v - avg)
	}
	return math.Sqrt(div / float64(len(xs)))
}

// percentile64
func percentile64(xs []float64, p float64) float64 {
	sort.Sort(sort.Float64Slice(xs))
	indexf := (float64(len(xs)) * p / 100) - 1.0
	index := int(indexf)
	return xs[index] + (indexf-float64(index))*(xs[index+1]-xs[index])
}
