package main

import (
	"math/rand"
	"os"
	"time"

	"github.com/mitchellh/cli"
)

const (
	ExitCodeOK        int = iota // 0
	ExitCodeError                // 1
	ExitCodeFileError            // 2
)

func main() {
	rand.Seed(time.Now().UTC().UnixNano())
	app := NewApp()
	c := cli.NewCLI(app.name, app.version)
	c.Args = os.Args[1:]
	c.Commands = map[string]cli.CommandFactory{
		// sucommand: statistics
		"statistics": func() (cli.Command, error) {
			return &Gosti{
				logger: app.logger,
			}, nil
		},
		// sucommand: categorize
		"categorize": func() (cli.Command, error) {
			return &GoCat{
				logger: app.logger,
			}, nil
		},
	}
	exitStatus, err := c.Run()
	if err != nil {
		app.logger.FatalLog(err)
	}
	os.Exit(exitStatus)
}
